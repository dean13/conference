<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'personal';

    protected $hidden = array(
        'created_at',
        'updated_at'
    );

    public $timestamps = false;
}
