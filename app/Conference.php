<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    protected $table = 'conference';

    protected $hidden = array(
        'created_at',
        'updated_at'
    );

    public $timestamps = false;
}
