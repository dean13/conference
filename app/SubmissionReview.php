<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionReview extends Model
{
    protected $table = 'submission_review';

    public $timestamps = false;
}
