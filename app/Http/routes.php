<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('start' ,'ConferenceController@show');
Route::get('start/miasto' , function() {
    return redirect('start');
});
/*
 * Sort conference
 */
//Route::get('start/city/{city}' , 'ConferenceController@sortConference');
//Route::get('start/category/{category}' , 'ConferenceController@sortConference');
Route::get('start/{sort}/{value}', 'ConferenceController@sortConference');
/*
 * Flash message
 */
Route::get('flash/{message}', function($message) {
    return redirect('start')->with('message_warning', $message);
});
Route::get('flashSuccess/{message}', function($message) {
    return redirect('start')->with('flash_message', $message);
});

/* User module */
Route::group(array('namespace' => 'User'), function() {
    Route::post('login', 'UserController@Login');
    Route::get('logout', 'UserController@Logout');
    Route::post('register', 'UserController@postRegister');
    Route::get('activate/{code}', 'UserController@activateAccount');
    Route::get('config', 'UserController@config');
    Route::post('changePassword', 'UserController@changePassword');
    Route::get('dashboard', 'UserController@dashboard');
    Route::post('update', 'UserController@update');

    Route::get('showUsers', 'UserController@showUsers');

    /*Settings user route*/
    Route::post('person', 'ConfigController@savePerson');
    Route::post('personData', 'ConfigController@loadPerson');
});

/*conference routes*/

Route::get('conference', 'ConferenceController@show');
Route::get('conferences', 'ConferenceController@showAll');
Route::get('conference/{id}', 'ConferenceController@showConference');
Route::post('conference', 'ConferenceController@store');
Route::get('add_conference', 'ConferenceController@viewAddConference');
Route::get('join', 'ConferenceController@joinToConference');


/*submission*/
Route::get('submission/{id}', 'SubmissionController@store');
Route::get('decision', 'SubmissionController@edit');
Route::get('send_report', 'SubmissionController@sendReport');
Route::get('check_decision', 'SubmissionController@checkDecision');
Route::get('conference/submission/{id}', 'SubmissionController@show');

/*review*/
Route::post('review/{id}', 'ReviewController@store');
Route::get('review_update/{id}', 'ReviewController@update');

Route::get('permission', function() {
    return back()->with('message_danger', 'Podstrona dostępna tylko po zalogowaniu !');
});

/*
 * Search
 */
Route::get('search', 'SearchController@index');

/*
 * Reset password
 */
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');


// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/*
 * committee invite
 */
Route::get('start/{email}', 'ConferenceController@registerAs');
//Route::get('restore', 'UserController@restorePassword');

