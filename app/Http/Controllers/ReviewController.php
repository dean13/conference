<?php

namespace App\Http\Controllers;

use App\Review;
use App\SubmissionReview;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DateTime;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - id submission
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $dt = new DateTime();
        $review = new Review();

        $review->id_user = Auth::user()->id;
        $review->opinion = $request->get('opinion');
        $review->date = $dt->format('Y-m-d');
        $review->experience_reviewer = $request->get('experience');
        $review->rating = $request->get('rate');
        $review->secret_attention = $request->get('attention');
        $review->save();

        $submissionReview = new SubmissionReview();
        $submissionReview->id_submission = $id;
        $submissionReview->id_review = $review->id;
        $submissionReview->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
