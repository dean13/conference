<?php

namespace App\Http\Controllers\User;

use App\Committee;
use App\Conference;
use App\Submission;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Auth;
use Hash;
use Input;
use Redirect;
use Validator;
use Mail;
use Session;
use Request;
use Response;
use ResetsPasswords;
use Crypt;
use App\Person;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    /**
     * @param $code - activation code
     * @return Redirect - return start site
     *
     * Method to activate already created account
     */
    public function activateAccount($code)
    {

        $user = User::where('verification_string', '=', substr($code, 1, -1))->first();

        if($user->activate==1) {
            return redirect('start')->with('flash_message', 'Twoje konto zostało już aktywowane');
        } else {
            $user->activate = 1;
            $user->save();
            return redirect('start')->with('flash_message', 'Sukces, twoje konto jest teraz aktywne i możesz się zalogować !');
        }
    }


    /**
     * @return mixed
     *
     * Return register site
     */
    public function getRegister()
    {
        return View::make('user.register');
    }

    /**
     * @return mixed
     *
     * Method to register a new user.
     */
    public function postRegister()
    {
        $rules = [
            'login' => 'required|alpha_num|between:3,32',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->passes()) {
            $validation_key = md5(rand() . Request::get('login') . Request::get('email') . rand()); // activation code generator

            $user = new User();
            $user->login = Request::get('login');
            $user->email = Request::get('email');
            $user->password = Hash::make(Request::get('password'));
            $user->verification_string = $validation_key;
            $user->save();

            // Email send
            $email = Request::get('email');
            Mail::send('emails.verify', ['key' => $validation_key], function ($message) use ($email) {
                $message->to($email)->subject('Witamy w Conference-system');
            }
            );

            return response()->json(['msg' => "Twoje konto zostało utworzone, jednak wymaga aktywacji"]);

        } else {
            return $validator->errors();
        }
        return $validator->errors()->all();
    }

    /**
     * @return string - return success if login is correct in other way return fail
     *
     * Method to login an user
     */
    public function Login()
    {

        $rules = [
            'login' => 'required',
            'password' => 'required',
        ];

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        } else {
            $userdata = array(
                'login' => Request::get('login'),
                'password' => Request::get('password')
            );
            // attempt to do the login
            if (Auth::attempt($userdata)) {
                return "success";
            } else {
                // validation not successful, send back to form
                return "fail";
            }

        }
    }

    /**
     * @return to start site with flash message
     *
     * Method to logout an active user
     */
    public function Logout()
    {
        Auth::logout();
        return redirect('/');
    }

    /**
     * @return redirect back with errors message
     *
     * Method to restore the password
     */
    public function restorePassword() {

        $rules = [
            'email' => 'required',
        ];

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {

            return $validator->errors();

        } else {
            $result = User::where('email', Request::get('email'))->first();

            if($result==null) {

                dd('nie');

            } else {
                //dd($result->password);

                $response = Password::sendResetLink(Request::get('email'), function($message)
                {
                    $message->subject('Password Reminder');
                });


                switch ($response)
                {
                    case PasswordBroker::RESET_LINK_SENT:
                        return redirect()->back()->with('flash_message', trans($response));

                    case PasswordBroker::INVALID_USER:
                        return redirect()->back()->withErrors(['email' => trans($response)]);
                }
            }
        }
    }

    /**
     * @return config blade site
     *
     * Return config blade site
     */
    public function config() {

        return View::make('users.config');
    }

    /**
     * @return response as a jason message with data
     *
     * Method to change the actual password to user login
     */
    public function changePassword() {

        $rules = [
            'actualPassword' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ];

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {

            return $validator->errors();

        } else {

            $userdata = array(
                'login' => Auth::user()->login,
                'password' => Request::get('actualPassword')
            );

            if (Auth::attempt($userdata)) {
                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make(Request::get('password'));
                    $user->save();

                    return response()->json(['msgOK' => "Pomyślnie zmieniłeś hasło do twojego konta"]);
            }
        }

    }

    /**
     * @return array - active (user account is activated), personal (user personal data are filled)
     */
    public function dashboard() {

        $activate = Auth::user()->activate;
        $personal = Person::where('id_user', '=', Auth::user()->id)->count();

        return array(['activate' => $activate, 'personal' => $personal]);
    }

    public function showUsers() {

        //$users = User::where('login','like','%'.Request::get('term').'%')->lists('login');
        $users = User::all()->lists('login','id');
        return $users;
    }

    public function update() {

        $rules = [
            'login' => 'required|alpha_num|between:3,32',
            'email' => 'required|email|unique:users'
        ];
        $validator = Validator::make(Request::all(), $rules);
        $validation_key = md5(rand() . Request::get('login') . Request::get('email') . rand());

        if ($validator->passes()) {

            $user = new User();
            $user->login = Request::get('login');
            $user->email = Request::get('email');
            $user->password = Hash::make(Request::get('password'));
            $user->verification_string = $validation_key;
            $user->activate = 1;
            $user->save();

            Committee::where('email', Request::get('email'))->update(['id_user' => $user->id]);

            return response()->json(['msg' => "Twoje konto zostało utworzone, aktywowane i przypisane jako recenzent"]);

        } else {
            return $validator->errors();
        }
        return $validator->errors()->all();


    }

    public static function isOrganizator($id_conference) {

        $organizator = Conference::where('id',$id_conference)->where('id_organizator',Auth::user()->id)->count();       //jeśli jest organizatorem
        if($organizator > 0) {
            //$submissions = Submission::where('id_conference',$id_conference)->get();
            return true;
        }

    }

    public static function statusPersonal($id_user) {

        if(Person::where('id_user',$id_user)->has('id_user')) {
            return true;
        }
    }
}
