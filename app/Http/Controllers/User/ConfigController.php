<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Redirect;
use Auth;
use App\Person;

class ConfigController extends Controller
{

    public function savePerson(Request $request) {

        $rules = [
            'name' => 'required|alpha|between:3,32',
            'surname' => 'required|alpha|between:3,32',
            'phone' => 'required|digits:9',
            'street' => 'required|alpha|between:3,32',
            'home' => 'required|alpha_num',
            'postcode' => 'required|alpha_dash:6',
            'town' => 'required|alpha|between:2,100',
            'country' => 'required|alpha'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }
        else {
            $person = new Person();
            $person->name = $request->get('name');
            $person->lastname = $request->get('surname');
            $person->phone = $request->get('phone');
            $person->street = $request->get('street');
            $person->home = $request->get('home');
            $person->postcode = $request->get('postcode');
            $person->town = $request->get('town');
            $person->country = $request->get('country');
            $person->birth_day = $request->get('date');
            $person->birth_day = $request->get('birth_day');
            $person->id_user = Auth::user()->id;
            $person->save();

            return response()->json(['msgOK' => "Dane personalne zostały pomyślnie zapisane."]);
        }

    }

    public function loadPerson() {

        return Person::where('id_user',Auth::user()->id)->first();
    }
}
