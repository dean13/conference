<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()) {

            $q = strtolower(Request::get('search'));
            $conference = DB::select('select * from conference where lower(title) like ? or lower(description) like ?', ['%'.$q.'%', '%'.$q.'%']);
            $submission = DB::select('select * from submission where lower(title) like ? or lower(describe_text) like ?', ['%'.$q.'%', '%'.$q.'%']);
            // dd($conference);

            $conferenceFounded = $this->replaceTable($conference, $q, 'conference');
            $submissionFounded = $this->replaceTable($submission, $q, 'submission');


            return view('search')->with(array('conferences' => $conferenceFounded, 'submissions' => $submissionFounded));
        }
        else {

            return redirect('permission');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - id konferencji
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function replaceTable(array $array, $query, $table_name) {


        $result = $this->stdClassToArray($array);

        $newArray = array();

        //dd($result);
        if($table_name==='conference') {
            foreach($result as $table) {

                $findT = str_ireplace($query, '<span class="highlight">'.$query.'</span>', $table[3]);
                $findD = str_ireplace($query, '<span class="highlight">'.$query.'</span>', $table[4]);
                array_set($table, '3', $findT);
                array_set($table, '4', $findD);
                array_push($newArray, $table);
            }
        }
        elseif($table_name==='submission') {
            foreach($result as $table) {

                $findT = str_ireplace($query, '<span class="highlight">'.$query.'</span>', $table[3]);
                $findD = str_ireplace($query, '<span class="highlight">'.$query.'</span>', $table[9]);
                array_set($table, '3', $findT);
                array_set($table, '9', $findD);
                array_push($newArray, $table);
            }
        }


        return $newArray;
    }

    /*
     * Convert object class of array to array list
     */
    public function stdClassToArray($arr) {
        $tables = array();

        foreach($arr as $table) {
            $temp = array();

            foreach($table as $col) {

                array_push($temp, $col);

            }
            array_push($tables, $temp);
        }
        return $tables;

    }
}