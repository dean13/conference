<?php

namespace App\Http\Controllers;

use App\Committee;
use App\Conference;
use App\ConferenceMember;
use App\Http\Controllers\User\UserController;
use App\Submission;
use Illuminate\Support\Facades\Input;
use Request;
use Session;
use Auth;
use Validator;
use Illuminate\Pagination;
use Mail;
use DB;

class ConferenceController extends Controller
{

    public function show() {

        if(Auth::check()) {


            $user_conference = DB::table('conference')
                ->select('*','conference_member.id_conference as id')
                ->join('conference_member','conference.id','=','conference_member.id_conference')
                ->where('conference_member.id_member',Auth::user()->id)
                ->paginate(5);
            $user_conference->setPath('start');


            return view('sites.conference_dashboard')->with('conference', $user_conference);
        }
        else {

            return view('sites.conference_dashboard');
        }


    }

    public function showAll() {

        $conferences = Conference::paginate(4);
        $conferences->setPath('conferences');

        return view('sites.conference_dashboard')->with('conference', $conferences);
    }

    public function store() {

        $rules = [
            'title' => 'required',
            'website' => 'required',
            'town' => 'required|alpha',
            'country' => 'required|alpha',
            'postcode' => 'required|alpha_dash:6',
            'street' => 'required',
            'begin' => 'required|date',
            'end' => 'required|date',
            'phone' => 'required|digits:9',
            'mail' => 'required|email',
            'description' => 'required',
            'committee' => 'required'
        ];


        //dd(Request::get('invite'));
        $validator = Validator::make(Request::all(), $rules);
        $validator2 = Validator::make(Request::all(), ['invite' => 'required']);

        if ($validator->fails() && $validator2->fails()) {

            return redirect('add_conference')->withInput()->withErrors($validator);

        } else {

            $conference = new Conference();
            $user_conference = Conference::all();
            $emailO = Request::get('mail');

            $conference->title = Request::get('title');
            $conference->website = Request::get('website');
            $conference->town = Request::get('town');
            $conference->country = Request::get('country');
            $conference->postcode = Request::get('postcode');
            $conference->street = Request::get('street');
            $conference->roles = implode("," ,Request::get('type'));
            $conference->begin = Request::get('begin');
            $conference->end = Request::get('end');
            $conference->website = Request::get('website');
            $conference->phone = Request::get('phone');
            $conference->mail = Request::get('mail');
            $conference->category = implode("," ,Request::get('category'));
            $conference->note = Request::get('note');
            $conference->description = Request::get('description');
            $conference->id_organizator = Auth::user()->id;
            $conference->save();

            $ConferenceMember = new ConferenceMember();
            $ConferenceMember->id_conference = $conference->id;
            $ConferenceMember->id_member = Auth::id();
            $ConferenceMember->save();

            if(is_null(Request::get('committee'))) {
                $invites = explode(PHP_EOL, Request::get('invite'));

                foreach ($invites as $invite) {

                    $committee = new Committee();
                    $committee->id_conference = $conference->id;
                    $committee->email = $invite;
                    $committee->save();

                    Mail::send('emails.invite', ['email' => $emailO, 'conference' => Request::get('title')], function ($message) use ($invite) {
                        $message->to($invite)->subject('Zaproszenie jako recenzet');
                    }
                    );
                }
            }
            else {
                foreach (Request::get('committee') as $user) {
                    $committee = new Committee();
                    $committee->id_user = $user;
                    $committee->id_conference = $conference->id;
                    $committee->email = isset(Request::get('committee')[1]);
                    $committee->save();
                }
            }

            return redirect('conference')->with(array('flash_message' => 'Pomyślnie zarejestrowano nowe wydarzenie', 'users' => $user_conference));
        }

    }

    public function showConference($id) {

        $conferenceData = Conference::where('id', $id)->first();
        $address = $conferenceData->country.", ".$conferenceData->town.", ".$conferenceData->street." ".$conferenceData->local;


        $isAdmin = UserController::isOrganizator($id);
        if($isAdmin == true) {
            $submissions = Submission::where('id_conference',$id)->get();
        }
        else {
            $submissions = Submission::where('id_conference',$id)->where('id_user',Auth::user()->id)->get();
            if(is_null($submissions)) {
                $submissions = Submission::where('id_conference',$id)->where('id_user',Auth::user()->id)->get();
            }
        }



        if(is_null($submissions)) {
            dd($submissions);
        }
        return view('sites.conference_active')->with(array('conf_info' => $conferenceData, 'mapa' => $address, 'submissions' => $submissions, 'id_conference' => $id));
    }

    public function viewAddConference() {

        $user = new UserController();
        $users = $user->showUsers();

        return view('sites.konferencje')->with('users', $users);
    }

    public function sortConference($sort, $value) {

        switch($sort) {
            case 'miasto': {

                $city = Conference::where('town', $value)->paginate(5);
                $city->setPath('start');

                return view('sites.conference_dashboard')->with('conference', $city);
                break;
            }
            case 'kategoria': {

                $category = Conference::where('category', 'like', '%'.$value.'%')->paginate(5);
                $category->setPath('start');

                return view('sites.conference_dashboard')->with('conference', $category);
                break;
            }
        }

    }

    public function registerAs($email) {

        Session::flash('message_warning', '<strong>Utwórz nowe konto teraz aby skorzystać z zaproszenia do roli recenzenta !</strong>');
        return view('menu')->with('email', $email);
    }

    public function joinToConference() {

        $member = new ConferenceMember();
        $member->id_conference = Input::get('conference');
        $member->id_member = Auth::user()->id;
        $member->save();

    }
}
