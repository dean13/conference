<?php

namespace App\Http\Controllers;

use App\Committee;
use App\Review;
use App\Submission;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Input;
use Storage;
use Mail;
use Validator;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - id konferencji
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'author' => 'required|alpha',
            'organization' => 'required|alpha',
            'deadline' => 'required|date',
        ];

        dd($request->file('file'));

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json($validator->errors());

        }

        $fileName = $request->file('file')->getClientOriginalName();

        $request->file('file')->move(
            base_path().'/public/assets/files/' , $fileName
        );
        $pathFile = '/assets/files/'.$fileName;

        $submission = new Submission();
        $submission->id_user = Auth::user()->id;
        $submission->title = $request->get('title');
        $submission->organization = $request->get('organization');
        $submission->date = Carbon::now()->toDateTimeString();
        $submission->file_path = $pathFile;
        $submission->id_conference = $id;
        $submission->describe_text = $request->get('describe');
        $submission->authors = $request->get('author');
        $submission->deadline = $request->get('deadline');
        $submission->save();

        return back()->with(array('flash_message' => 'Pomyślnie dodano artykuł'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()) {

            $submission = Submission::where('id',$id)->first();
            $conference_id = Submission::where('id',$id)->first()->pluck('id_conference');
            $reviews = DB::table('review')
                ->join('submission_review', 'review.id', '=', 'submission_review.id_review')
                ->join('submission', 'submission.id', '=', 'submission_review.id_submission')
                ->select('*', 'review.id as id_review', 'review.id_user as id_uzytkownika')
                ->where('submission.id', '=', $id)
                ->get();

            $committee = Committee::whereRaw('id_user = ? and id_conference = ?', array(Auth::user()->id, $conference_id))->get();

            $raportPDF = Submission::where('id', $id)->get()->first()->file_path;
            //dd($raportPDF);
            //$contents = Storage::get('storage/mojaa_sieeeeec.doc');
            $userReview = DB::table('review')
                ->join('submission_review', 'review.id', '=', 'submission_review.id_review')
                ->where('review.id_user', '=', Auth::user()->id)
                ->where('submission_review.id_submission', '=', $id)
                ->get();



            if(Carbon::now() > $submission->deadline) {
                Session::flash('message_warning', 'Upłynął Deadline dodawania recenzji do tego artykułu');

                $rate = DB::table('review')
                    ->select(DB::raw('sum(review.rating) * review.experience_reviewer / sum(review.experience_reviewer) as rate'))
                    ->join('submission_review', 'submission_review.id_review', '=', 'review.id')
                    ->where('submission_review.id_submission', '=', $id)
                    ->pluck('rate');

                $secretMessage = $this->secretMessage();

                return view('sites.submission')->with(array('submission' => $submission, 'reviews' => $reviews, 'committee_member' => $committee, 'PDF' => $raportPDF,
                    'userReview' => $userReview, 'rate' => $rate, 'message' => $secretMessage));
            }
            else {
                return view('sites.submission')->with(array('submission' => $submission, 'reviews' => $reviews, 'committee_member' => $committee, 'PDF' => $raportPDF,
                    'userReview' => $userReview));
            }

        }
        else {
            return redirect('permission');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $submission = Submission::find(Input::get('index'));
        $submission->decision = Input::get('decision');
        $submission->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendReport() {

        $data = DB::table('submission')
            ->select('*', 'users.email', 'conference.title as conference_title', 'submission.title as submission_title')
            ->join('users', 'submission.id_user', '=', 'users.id')
            //->join('personal', 'submission.id_user', '=', 'personal.id_user')
            ->join('conference', 'submission.id_conference', '=', 'conference.id')
            ->where('submission.id_conference','=',Input::get('conference'))
            ->get();


        foreach($data as $email) {

            Mail::send('emails.report', ['submissions' => $data], function ($message) use ($email) {
                $message->to($email->email)->subject('Raport artykułów z konferencji '.'"'.$email->conference_title.'"');
            }
            );
        }

    }

    public function checkDecision() {
        if(Submission::where('id_conference', Input::get('conference'))->whereNull('decision')->count() > 0 ) {
            return 1;
        }
    }

    public function secretMessage() {

        $review_message = DB::table('review')
            ->select('*')
            ->join('submission_review','submission_review.id_review','=','review.id')
            ->join('submission','submission.id','=','submission_review.id_submission')
            ->join('users','users.id','=','review.id_user')
            ->where('review.id_user','!=',Auth::user()->id)
            ->get();

        return $review_message;
    }

}
