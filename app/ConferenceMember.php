<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConferenceMember extends Model
{
    protected $table = 'conference_member';


    public $timestamps = false;
}
