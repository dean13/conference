-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Lip 2016, 23:49
-- Wersja serwera: 10.1.10-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `conference`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `committee`
--

CREATE TABLE `committee` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_conference` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `committee`
--

INSERT INTO `committee` (`id`, `id_conference`, `id_user`, `email`) VALUES
(1, 2, 1, '0'),
(2, 4, 1, '0'),
(3, 5, 1, '0');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `conference`
--

CREATE TABLE `conference` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` int(11) NOT NULL,
  `begin` date NOT NULL,
  `end` date NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_organizator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `conference`
--

INSERT INTO `conference` (`id`, `title`, `website`, `town`, `country`, `postcode`, `street`, `roles`, `begin`, `end`, `phone`, `mail`, `category`, `note`, `description`, `id_organizator`) VALUES
(1, 'Draggable modal Bootstrap 3', 'fdsfsdf', 'Żołynia', 'Polska', '37-110', 'Mickiewicza', 0, '2016-07-14', '2016-07-16', '784202512', 'alan.urban23@gmail.com', '0,2', 'dfdsfsdfsd', 'ksdjfkdsjhgkjfdgfd', 1),
(3, 'Nowa konferencja', 'fdsfsdf', 'Żołynia', 'Polska', '37-110', 'Mickiewicza', 0, '2016-07-14', '2016-07-23', '784202512', 'Blade2821@wp.pl', '3', 'fgfdgfdsagdfgfdgdg', 'Bardzo dobra ', 1),
(5, 'Czy zadziała', 'fdsfsdf', 'Kraków', 'Polska', '33-332', 'Antoniego Gramatyka 8A', 0, '2016-07-07', '2016-07-30', '515254333', 'd.a.galuszka@gmail.com', '1,4', 'fdgfdgdfgdfg', 'Co ja wogóle robię', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `conference_member`
--

CREATE TABLE `conference_member` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_conference` int(11) NOT NULL,
  `id_member` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `conference_member`
--

INSERT INTO `conference_member` (`id`, `id_conference`, `id_member`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 4, 1),
(4, 5, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_07_19_202537_UsersTable', 1),
('2016_07_19_202715_CommitteeTable', 1),
('2016_07_19_202725_ConferenceTable', 1),
('2016_07_19_202736_PersonTable', 1),
('2016_07_19_202745_ReviewTable', 1),
('2016_07_19_202754_SubmissionTable', 1),
('2016_07_19_202803_SubmissionReviewTable', 1),
('2016_07_19_202814_UserRolesTable', 1),
('2016_07_19_210150_ConferenceMemberTable', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `personal`
--

CREATE TABLE `personal` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_day` date NOT NULL,
  `date` date NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `personal`
--

INSERT INTO `personal` (`id`, `name`, `lastname`, `phone`, `street`, `home`, `postcode`, `town`, `country`, `birth_day`, `date`, `id_user`) VALUES
(1, 'Alan', 'Urban', '784202512', 'Mickiewicza', '33', '37-110', 'Żołynia', 'Polska', '2016-01-13', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `review`
--

CREATE TABLE `review` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `opinion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `experience_reviewer` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `secret_attention` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `submission`
--

CREATE TABLE `submission` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_conference` int(11) NOT NULL,
  `describe_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authors` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `submission_review`
--

CREATE TABLE `submission_review` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_submission` int(11) NOT NULL,
  `id_review` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verification_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `password`, `verification_string`, `created_at`, `updated_at`) VALUES
(1, 'dean13', 'alan.urban23@gmail.com', '$2y$10$fNUyrqqBEmwKlS9c9BODY.EXPWJ.bI2LhHpHqXaH5BIjFIdMWQ65W', '1a0824103d02b5a89da7f99f8a79a453', '2016-07-19 18:57:50', '2016-07-19 18:57:50');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `committee`
--
ALTER TABLE `committee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conference`
--
ALTER TABLE `conference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conference_member`
--
ALTER TABLE `conference_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submission`
--
ALTER TABLE `submission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submission_review`
--
ALTER TABLE `submission_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `committee`
--
ALTER TABLE `committee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `conference`
--
ALTER TABLE `conference`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `conference_member`
--
ALTER TABLE `conference_member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `personal`
--
ALTER TABLE `personal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `review`
--
ALTER TABLE `review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `submission`
--
ALTER TABLE `submission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `submission_review`
--
ALTER TABLE `submission_review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
