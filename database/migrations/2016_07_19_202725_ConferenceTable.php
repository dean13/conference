<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conference', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('website');
            $table->string('town');
            $table->string('country');
            $table->string('postcode');
            $table->string('street');
            $table->integer('roles');
            $table->date('begin');
            $table->date('end');
            $table->string('phone');
            $table->string('mail');
            $table->string('category');
            $table->string('note');
            $table->string('description');
            $table->integer('id_organizator');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conference');
    }
}
