@extends('main')
@section('flash')

@if(Session::has('flash_message'))
    <div class="alert alert-flash alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p>{!! Session::get('flash_message') !!}</p>
    </div>
@endif

@if(Session::has('message_warning'))
    <div class="alert alert-flash alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p>{!! Session::get('message_warning') !!}</p>
    </div>
@endif

@if(Session::has('message_info'))
    <div class="alert alert-flash alert-dismissible alert-info">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <p>{!! Session::get('message_info') !!}</p>
    </div>
@endif

@if(Session::has('message_danger'))
    <div class="alert alert-flash alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <p>{!! Session::get('message_danger') !!}</p>
    </div>
@endif

<script>
    $(document).ready(function () {
       $('.alert-flash').delay(3000).slideUp(300);
    });
</script>

@endsection