<!DOCTYPE html>
<html lang="pl-PL">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Witamy w conference-system</h2>

<div>
    Dziękujemy za założenie nowego konta w systemie conference-system. Aby w pełni wykorzystać swoje konto, aktywuj je poprzez link podany poniżej !
    <br/>
    {!! URL::to('activate', '{'.$key.'}') !!}.
</div>

</body>
</html>