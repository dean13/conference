<!DOCTYPE html>
<html lang="pl-PL">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <table class="table">
        <thead class="review_table">
        <tr>
            <th>Tytuł</th>
            <th>Deadline</th>
            <th>Autorzy</th>
            <th style="text-align: center">Decyzja</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="submission_table">
        @foreach($submissions as $submission)
            <tr>
                <td>{!! $submission->submission_title !!}</td>
                <td>{!! $submission->deadline !!}</td>
                <td>{!! $submission->authors !!}</td>
                @if($submission->decision == 1)
                    <td>Zatwierdzony</td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>