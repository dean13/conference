@include('main')

 <nav class="navbar navbar-default navbar-menu">
            <div class="navbar-header">
                <div class="btn-group">
                    <a class="navbar-brand" href="{!! URL::to('start') !!}">ConferenceSystem</a>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! URL::to('conferences') !!}" style="font-size: 0.9em">Przeglądaj konferencje</a></li>
                    </ul>
                </div>
            </div>

            <div class="collapse navbar-collapse">

                {!! Form::open(array('method' => 'get', 'url' => URL::to('search'), 'class' => 'navbar-form navbar-left', 'id' => 'form-menu')) !!}
                    <div class="search-div">
                        <div class="form-group">
                            {!! Form::text('search', null, array('class' => 'form-control', 'placeholder' => 'Szukaj frazę')) !!}
                        </div>
                        {!! Form::button( '<span class="glyphicon glyphicon-search"></span>',  array('class'=>'btn btn-default','type'=>'submit')) !!}
                    </div>
                {!! Form::close() !!}

                <form class="nav navbar-form navbar-right">
                    @if(Auth::check())
                        <div id="circle" role="button" data-toggle="popover" id="popover"><span class="glyphicon glyphicon-user user_picture" aria-hidden="true"></span></div>
                    @else
                            <button type="button" class="btn btn-success button-menu" data-toggle="modal" data-target="#loginModal">Zaloguj</button>
                            <button type="button" class="btn btn-default button-menu" data-toggle="modal" data-target="#registerModal" id="register">Dołącz</button>
                    @endif
                </form>
            </div>
    </nav>


 <!-- Modal -->
 <!-- LOGOWANIE -->
 <div class="modal" id="loginModal">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Logowanie do ConferenceSystem</h4>
             </div>
             <div class="modal-body">

                 {!! Form::open(array('method' => 'post', 'url' => URL::to('login'), 'class' => 'form-horizontal', 'id' => 'loginForm')) !!}
                 <fieldset>
                     <div class="form-group">
                         {!! Form::label('inputEmail', 'Login', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             {!! Form::text('login', null, array('class' => 'form-control', 'id' => 'inputEmail')) !!}
                         </div>
                     </div>
                     <div class="form-group">
                         {!! Form::label('inputPassword', 'Hasło', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             {!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword')) !!}
                         </div>
                     </div>
                 </fieldset>

             </div>
             <div class="modal-footer">
                 <div class="warning">
                     <ul class="list-unstyled">

                     </ul>
                 </div>

                 <a href="{!! URL::to('restore') !!}"><span class="label label-default" data-toggle="modal" data-target="#forgetPasswordModal" data-dismiss="modal">Zapomniałem hasła</span></a>
                 {!! Form::submit('Zaloguj', array('class' => 'btn btn-primary loginButton')) !!}
             </div>
             {!! Form::close() !!}
         </div>
     </div>
 </div>



 <div class="modal" id="forgetPasswordModal">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Odzyskiwanie hasła</h4>
             </div>
             <div class="modal-body">

                 {!! Form::open(array('method' => 'post', 'url' => '/password/email', 'class' => 'form-horizontal')) !!}
                 <fieldset>
                     <div class="form-group">
                         {!! Form::label('email', 'email', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             {!! Form::email('email', null, array('class' => 'form-control', 'id' => 'inputEmail')) !!}
                         </div>
                     </div>
                 </fieldset>

             </div>
             <div class="modal-footer">
                 {!! Form::submit('Odzyskaj', array('class' => 'btn btn-primary')) !!}
             </div>
             {!! Form::close() !!}
         </div>
     </div>
 </div>


 <div class="modal" id="registerModal">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Rejestrowanie użytkownika</h4>
             </div>
             <div class="modal-body">

                 @if(isset($email))
                     {!! Form::open(array('method' => 'post', 'url' => 'update', 'class' => 'form-horizontal', 'id' => 'updateForm')) !!}
                 @else
                     {!! Form::open(array('method' => 'post', 'url' => 'register', 'class' => 'form-horizontal', 'id' => 'registerForm')) !!}
                 @endif
                 <fieldset>
                     <div class="form-group">
                         {!! Form::label('inputLogin', 'Login', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             {!! Form::text('login', null, array('class' => 'form-control', 'id' => 'inputLogin')) !!}
                         </div>
                     </div>

                     <div class="form-group">
                         {!! Form::label('inputPassword', 'Hasło', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             {!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword')) !!}
                         </div>
                     </div>
                     <div class="form-group">
                         {!! Form::label('inputEmail', 'e-mail', array('class' => 'col-lg-2 control-label')) !!}
                         <div class="col-lg-10">
                             @if(isset($email))
                                 {!! Form::email('email', substr($email, 1, -1), array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Email', 'readonly' => '')) !!}
                             @else
                                 {!! Form::email('email', null, array('class' => 'form-control', 'id' => 'inputEmail')) !!}
                             @endif
                         </div>
                     </div>

                 </fieldset>

             </div>
             <div class="modal-footer">

                 <div class="warning">
                     <ul class="list-unstyled">

                     </ul>
                 </div>

                 {!! Form::submit('Załóż', array('class' => 'btn btn-primary registerButton')) !!}
             </div>

             {!! Form::close() !!}
         </div>
     </div>
 </div>





 <script type="text/javascript">
     $(document).ready(function(){

         var loginForm = $("#loginForm");
         var registerForm = $("#registerForm");
         var updateForm = $("#updateForm");
         var contentHtml = '<a href="config" class="accountConfig">Zarządzanie kontem <span class="glyphicon glyphicon-cog"></span></a>' +
                 '<a href="{!! URL::to('logout') !!}"><button type="button" class="btn btn-primary logoutBtn">Wyloguj</button></a>';
         var titlePopover;
         @if(Auth::user())
             titlePopover = 'Zalogowany: {!! Auth::user()->login !!}';
         @endif

         /*
          * Initialize popovers
          */
         $('[data-toggle="popover"]').popover({
             title: titlePopover,
             placement: 'bottom',
             html: true,
             content: contentHtml,
         });


         loginForm.submit(function(e){
             e.preventDefault();
             var formData = loginForm.serialize();
             $('#loginModal .warning li').remove();

             $.ajax({
                 url:'{!! URL::to('login') !!}',
                 method:'post',
                 data:formData,
                 success:function(data){

                     if(data==="success") {
                         window.location.reload();
                     }
                     else if(data==="fail") {
                         $('#loginModal .warning ul').append('<li>Dane nie poprawne</li>')
                     }
                     else {
                         $.each( data, function( key, value ) {
                             $('#loginModal .warning ul').append('<li>'+value+'</li>')
                         });
                     }
                 },
                 error: function (data) {
                 }
             });
         });


         registerForm.submit(function(e){
             e.preventDefault();
             var formData = registerForm.serialize();
             $('#registerModal .warning li').remove();

             $.ajax({
                 url:'{!! URL::to('register') !!}',
                 method:'post',
                 data:formData,
                 success:function(data){

                     if(data['msg']) {
                         var message = data['msg'];
                         window.location.href = "flash/"+message;
                     }
                     else {
                         $.each( data, function( key, value ) {
                             //console.log(value);
                             $('#registerModal .warning ul').append('<li>'+value+'</li>')
                         });
                     }
                 },
                 error: function (data) {
                     //console.log(data);
                 }
             });
         });

         updateForm.submit(function(e){
             e.preventDefault();
             var formData = updateForm.serialize();
             $('#registerModal .warning li').remove();

             $.ajax({
                 url:'{!! URL::to('update') !!}',
                 method:'post',
                 data:formData,
                 success:function(data){

                     if(data['msg']) {
                         var message = data['msg'];
                         window.location.href = "flash/"+message;
                     }
                     else {
                         $.each( data, function( key, value ) {
                             //console.log(value);
                             $('#registerModal .warning ul').append('<li>'+value+'</li>')
                         });
                     }
                 },
                 error: function (data) {
                     //console.log(data);
                 }
             });
         });

     });
 </script>