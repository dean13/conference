@include('main')
@include('menu')

<div class="container">
    <div class="page-header">
        <h1 id="navbar">Konferencje</h1>
    </div>
    <div class="list-group list-conference">
        <ul class="list-group">
            @foreach($conferences as $c)
                <li class="list-group-item">
                    <h4><strong><a href="{!! URL::to('conference', $c[0]) !!}">{!! $c[3] !!}</a></strong></h4>
                    <div class="list-content">{!! $c[4] !!}</div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="page-header">
        <h1 id="navbar">Artykuły</h1>
    </div>
    <div class="list-group list-submission">
        <ul class="list-group">
            @foreach($submissions as $s)
                <li class="list-group-item">
                    <h4><strong><a href="{!! URL::to('conference/submission', $s[0]) !!}">{!! $s[3] !!}</a></strong></h4>
                    <div class="list-content">{!! $s[9] !!}</div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
