@include('main')
@include('menu')

<div class="container">
    <div class="bs-docs-section">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1 id="navs">Ustawienia konta</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2">
                <ul class="nav nav-pills nav-stacked nav-settings">
                    <li class="active"><a href="#dashboard" data-toggle="tab" aria-expanded="true"><span class="glyphicon glyphicon-home"></span> Podsumowanie</a></li>
                    <li class=""><a href="#personal" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Dane personalne</a></li>
                    <li class=""><a href="#password" data-toggle="tab" aria-expanded="true"><span class="glyphicon glyphicon-lock"></span> Zmiana hasła</a></li>
                </ul>
            </div>
            <div class="col-lg-9">
                <div id="myTabContent" class="tab-content">

                    <div class="tab-pane fade col-lg-10" id="dashboard" style="text-align: left">

                        <div class="row status-row">
                            <div class="panel panel-default panel-status">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Status konta</h3>
                                </div>
                                <div class="panel-body">

                                </div>
                            </div>
                        </div>

                        <div class="row personal-row">
                            <div class="panel panel-default panel-personal">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Dane personalne</h3>
                                </div>
                                <div class="panel-body">

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade col-lg-10" id="personal">
                        {!! Form::open(array('method' => 'post', 'url' => 'person', 'class' => 'form-horizontal', 'id' => 'savePerson')) !!}
                        <div class="form-group form-name">
                            {!! Form::label('name', 'Imię', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-name col-lg-6">{!! Form::text('name', null, array('class' => 'form-control', 'id' => 'name')) !!}</div>
                        </div>
                        <div class="form-group form-surname">
                            {!! Form::label('surname', 'Nazwisko', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-surname col-lg-6">{!! Form::text('surname', null, array('class' => 'form-control', 'id' => 'lastname')) !!}</div>
                        </div>
                        <div class="form-group form-surname date" id="datePicker">
                            {!! Form::label('birth_day', 'Data urodzenia', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-birthday col-lg-6">
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('birth_day', null, array('class' => 'form-control', 'id' => 'birth_day')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('website', 'Wizytówka online', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-website col-lg-6">{!! Form::text('website', null, array('class' => 'form-control col-lg-4', 'id' => 'website')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', 'Nr telefonu', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-phone col-lg-6">{!! Form::text('phone', null, array('class' => 'form-control', 'id' => 'phone')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('street', 'Ulica', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-street col-lg-6">{!! Form::text('street', null, array('class' => 'form-control', 'id' => 'street')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('home', 'Nr domu/mieszkania', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-home col-lg-6">{!! Form::text('home', null, array('class' => 'form-control', 'id' => 'home')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('postcode', 'Kod pocztowy', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-postcode col-lg-6">{!! Form::text('postcode', null, array('class' => 'form-control', 'id' => 'postcode')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('town', 'Miasto', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-town col-lg-6">{!! Form::text('town', null, array('class' => 'form-control', 'id' => 'town')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('country', 'Kraj', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-country col-lg-6">{!! Form::text('country', null, array('class' => 'form-control', 'id' => 'country')) !!}</div>
                        </div>

                        <div class="form-group">

                            <div class="col-lg-8 col-lg-offset-2">
                                {!! Form::reset('ANULUJ', array('class' => 'btn btn-default cancelButton')) !!}
                                {!! Form::submit('ZAPISZ', array('class' => 'btn btn-primary saveButton')) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>


                    <div class="tab-pane fade col-lg-10" id="password">
                        {!! Form::open(array('method' => 'post', 'url' => 'changePassword', 'class' => 'form-horizontal', 'id' => 'changePassword')) !!}
                        <div class="form-group">
                            {!! Form::label('actualPassword', 'Aktualne hasło', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-actualPassword col-lg-4">{!! Form::password('actualPassword', null, array('class' => 'form-control', 'id' => 'actualPassword')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Nowe hasło', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-newPassword col-lg-4">{!! Form::password('password', null, array('class' => 'form-control', 'id' => 'password')) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Potwierdź hasło', array('class' => 'control-label col-lg-3')) !!}
                            <div class="input-form input-applyPassword col-lg-4">{!! Form::password('password_confirmation', null, array('class' => 'form-control', 'id' => 'password_confirmation')) !!}</div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-11 col-lg-offset-1">
                                {!! Form::reset('ANULUJ', array('class' => 'btn btn-default cancelButton')) !!}
                                {!! Form::submit('ZMIEŃ HASŁO', array('class' => 'btn btn-primary resetPassword')) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function(){


        function reloadWithData(data) {
            var message = data['msgOK'];
            window.location.href = "flashSuccess/"+message;
        }
        function loadDashboard() {

            $.ajax({
                url: 'dashboard',
                method: 'get',
                success: function (data) {
                    $.each( data[0], function( key, value ) {
                        switch (key) {

                            case 'activate': {
                                if (value==1) {
                                    $(".panel-status").attr('class', 'panel panel-success');
                                    $('.status-row .panel-body').text('Konto zostało aktywowane możesz przystąpić teraz do uzupełnienia swojego profilu');
                                }
                                else {
                                    $(".panel-status").attr('class', 'panel panel-warning');
                                    $('.status-row .panel-body').text('Konto nie jest w pełni aktywowane, konto jest bardzo ograniczone ! Aby w pełni aktywować konto potwierdź odnośnik z maila');
                                }
                                break;
                            }
                            case 'personal': {
                                if (value==1) {
                                    $(".panel-personal").attr('class', 'panel panel-success');
                                    $('.personal-row .panel-body').text('Dane personalne zostały uzupełnione, możesz brać udział w konferencjach.');
                                }
                                else {
                                    $(".panel-personal").attr('class', 'panel panel-warning');
                                    $('.personal-row .panel-body').html('Aby w pełni wykorzystać swoje konto, uzupełnij swoje dane personalne.');
                                }
                                break;
                            }
                        }

                    });

                }
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var personForm = $("#savePerson");
        var passwordForm = $("#changePassword");

        $('#datePicker')
                .datepicker({
                    format: 'yyyy/mm/dd'
                }).on('changeDate', function(ev){
                    $('#datePicker').datepicker('hide');
                });

        //load data to person form
        $('ul li a[href="#personal"]').click(function() {

            $.ajax({
                url: 'personData',
                method: 'post',
                success: function (data) {
                    //console.log(data[0]['name']);
                    $.each( data, function( key, value ) {
                        //$('#organization option[value="'+value+'"]').attr('selected','selected');
                        //console.log(key+" "+value);
                        $('input[id="'+key+'"]').val(value);

                    });

                }
            });
        });

        //load data to dashboard
        $('ul li a[href="#dashboard"]').click(function() {

            loadDashboard();
        });
        $('.accountConfig').click(function () {


        });


        passwordForm.submit(function(e){
            e.preventDefault();
            var formData = passwordForm.serialize();
            //$('#personal .alert').remove();

            $.ajax({
                url: 'changePassword',
                method: 'post',
                data: formData,
                success: function (data) {
                    console.log(data);
                    if (data['msgOK']) {
                        reloadWithData(data);
                    }
                    else {
                        $.each( data, function( key, value ) {

                            if (key=='password') {
                                $('#password .input-newPassword').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>');
                                alert('ok');
                            }
                            else if (key=='password_confirmation') {
                                $('#password .input-applyPassword').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>');
                                alert('ok');
                            }
                        });
                    }
                }
            });

        });

        personForm.submit(function(e){
            e.preventDefault();
            var formData = personForm.serialize();
            $('#personal .alert').remove();

            $.ajax({
                url:'person',
                method:'post',
                data:formData,
                success:function(data){

                    if(data['msgOK']) {
                        var message = data['msgOK'];
                        window.location.href = "flashSuccess/"+message;
                    }
                    else {
                        $.each( data, function( key, value ) {
                            switch (key) {
                                case 'name': {
                                    $('#personal .input-name').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'surname': {
                                    $('#personal .input-surname').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'phone': {
                                    $('#personal .input-phone').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'street': {
                                    $('#personal .input-street').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'home': {
                                    $('#personal .input-home').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'postcode': {
                                    $('#personal .input-postcode').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'town': {
                                    $('#personal .input-town').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                                case 'country': {
                                    $('#personal .input-country').append('<div class="alert alert-dismissible alert-danger">'+value+'</div>')
                                    break;
                                }
                            }
                            $('#personal .warning ul').append('<li>'+value+'</li>')
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });

    });

</script>


