<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="ISO-8859-1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="../../favicon.ico">

    <title>Laravel</title>
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="assets/css/cover.css" rel="stylesheet" type="text/css">

    <link href="assets/css/general.css" rel="stylesheet" type="text/css">
</head>


<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">ConferenceSystem</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Start</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Kontakt</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">
                <h1 class="cover-heading">Co to za system ?</h1>
                <p class="lead">Jest to globalny system służacy do zarządzania konferencjami naukowymi. Od przeglądania dostępnych konfernecji jako uczestnik aż do zarządzania nimi jako recenzent</p>
                <p class="lead">
                    <a href="" class="btn btn-lg btn-default" data-toggle="modal" data-target="#loginModal">Zaloguj </a>
                    <a href="" class="btn btn-lg btn-default" id="btn-register" data-toggle="modal" data-target="#registerModal">Załóż konto !</a>
                </p>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Wszystkie prawa zastrzeżone <a href="http://getbootstrap.com">2015</a>, przez <a href="https://twitter.com/mdo">ConferenceSystem</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- LOGOWANIE -->
<div class="modal" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Logowanie do ConferenceSystem</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(array('method' => 'post', 'url' => URL::to('login'), 'class' => 'form-horizontal', 'id' => 'loginForm')) !!}
                <fieldset>
                    <div class="form-group">
                        {!! Form::label('inputEmail', 'Login', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            {!! Form::text('login', null, array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Login')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputPassword', 'Password', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            {!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Password')) !!}
                        </div>
                    </div>
                </fieldset>

            </div>
            <div class="modal-footer">
                <div class="warning">
                    <ul class="list-unstyled">

                    </ul>
                </div>

                <a href=""><span class="label label-default" data-toggle="modal" data-target="#forgetPasswordModal" data-dismiss="modal">Zapomniałem hasła</span></a>
                {!! Form::submit('Zaloguj', array('class' => 'btn btn-primary loginButton')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



<div class="modal" id="forgetPasswordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Odzyskiwanie hasła</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(array('method' => 'post', 'url' => 'password/email', 'class' => 'form-horizontal', 'id' => 'restoreForm')) !!}
                <fieldset>
                    <div class="form-group">
                        {!! Form::label('email', 'email', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            {!! Form::email('email', null, array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Email')) !!}
                        </div>
                    </div>
                </fieldset>

            </div>
            <div class="modal-footer">
                <div class="warning">
                    <ul class="list-unstyled">

                    </ul>
                </div>

                {!! Form::submit('Odzyskaj', array('class' => 'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal" id="registerModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Rejestrowanie użytkownika</h4>
            </div>
            <div class="modal-body">

                @if(isset($email))
                    {!! Form::open(array('method' => 'post', 'url' => 'update', 'class' => 'form-horizontal', 'id' => 'updateForm')) !!}
                @else
                    {!! Form::open(array('method' => 'post', 'url' => 'register', 'class' => 'form-horizontal', 'id' => 'registerForm')) !!}
                @endif
                <fieldset>
                    <div class="form-group">
                        {!! Form::label('inputLogin', 'Login', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            {!! Form::text('login', null, array('class' => 'form-control', 'id' => 'inputLogin', 'placeholder' => 'Login')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('inputPassword', 'Hasło', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            {!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Hasło')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputEmail', 'e-mail', array('class' => 'col-lg-2 control-label')) !!}
                        <div class="col-lg-10">
                            @if(isset($email))
                                {!! Form::email('email', substr($email, 1, -1), array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Email', 'readonly' => '')) !!}
                            @else
                                {!! Form::email('email', null, array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Email')) !!}
                            @endif
                        </div>
                    </div>

                </fieldset>

            </div>
            <div class="modal-footer">

                <div class="warning">
                    <ul class="list-unstyled">

                    </ul>
                </div>

                {!! Form::submit('Załóż', array('class' => 'btn btn-primary registerButton')) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        var loginForm = $("#loginForm");
        var registerForm = $("#registerForm");
        var restoreForm = $("#restoreForm");
        var updateForm = $("#updateForm");

        var contentHtml = '<a href="config" class="accountConfig">Zarządzanie kontem <span class="glyphicon glyphicon-cog"></a>' +
                '<a href="{!! URL::to('logout') !!}"><button type="button" class="btn btn-primary logoutBtn">Wyloguj</button></a>';
        var titlePopover;
        @if(Auth::user())
            titlePopover = 'Zalogowany: {!! Auth::user()->login !!}';
        @endif

        /*
         * Initialize popovers
         */
        $('[data-toggle="popover"]').popover({
            title: titlePopover,
            placement: 'bottom',
            html: true,
            content: contentHtml,
        });


        loginForm.submit(function(e){
            e.preventDefault();
            var formData = loginForm.serialize();
            $('#loginModal .warning li').remove();

            $.ajax({
                url:'{!! URL::to('login') !!}',
                method:'post',
                data:formData,
                success:function(data){

                    if(data==="success") {
                        window.location.href = '{!! URL::to('start') !!}';
                    }
                    else if(data==="fail") {
                        $('#loginModal .warning ul').append('<li>Dane nie poprawne</li>')
                    }
                    else {
                        $.each( data, function( key, value ) {
                            $('#loginModal .warning ul').append('<li>'+value+'</li>')
                        });
                    }
                },
                error: function (data) {
                }
            });
        });


        registerForm.submit(function(e){
            e.preventDefault();
            var formData = registerForm.serialize();
            $('#registerModal .warning li').remove();

            $.ajax({
                url:'{!! URL::to('register') !!}',
                method:'post',
                data:formData,
                success:function(data){

                    if(data['msg']) {
                        var message = data['msg'];
                        window.location.href = "flash/"+message;
                    }
                    else {
                        $.each( data, function( key, value ) {
                            //console.log(value);
                            $('#registerModal .warning ul').append('<li>'+value+'</li>')
                        });
                    }
                },
                error: function (data) {
                    //console.log(data);
                }
            });
        });


        restoreForm.submit(function(e){
            e.preventDefault();
            var formData = restoreForm.serialize();
            $('#forgetPasswordModal .warning li').remove();

            $.ajax({
                url:'{!! URL::to('password/email') !!}',
                method:'post',
                data:formData,
                success:function(data){
                    $('#forgetPasswordModal .warning ul').append('<li>'+data+'</li>')
                },
                error: function (data) {
                    //console.log(data);
                }
            });
        });

        updateForm.submit(function(e){
            e.preventDefault();
            var formData = updateForm.serialize();
            $('#registerModal .warning li').remove();

            $.ajax({
                url:'{!! URL::to('update') !!}',
                method:'post',
                data:formData,
                success:function(data){

                    if(data['msg']) {
                        var message = data['msg'];
                        window.location.href = "flash/"+message;
                    }
                    else {
                        $.each( data, function( key, value ) {
                            //console.log(value);
                            $('#registerModal .warning ul').append('<li>'+value+'</li>')
                        });
                    }
                },
                error: function (data) {
                    //console.log(data);
                }
            });
        });

    });
</script>