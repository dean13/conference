@include('flash')
@include('main')
@include('menu')

@yield('flash')

    <div class="container" id="container-body">
        <div class="page-header">
            <h1 id="navbar">Konferencje w których bierzesz udział</h1>
        </div>

            @if(isset($conference))
                <div class="container container-middle">

                    <div class="list-group list-conference">
                        {!! $conference->render() !!}
                        <ul class="list-unstyled">
                            @foreach($conference as $event)
                                <li>
                                    <h4 class="list-group-item-heading">{!! $event->title !!}</h4>
                                    <p class="list-group-item-heading">Rola - {!! $event->roles !!}</p>
                                    <p class="list-group-item-heading">Kategorie -
                                        @foreach(explode(',', $event->category) as $category) <a href="{!! URL::to('start/kategoria', $category) !!}">{!! $category !!}</a> , @endforeach</p>
                                    <p class="list-group-item-heading">Data - {!! $event->begin !!}</p>
                                    <p class="list-group-item-text">Lokalizacja - {!! $event->town !!}</p>
                                    <a href="{!! URL::to('conference', $event->id) !!}"><button type="button" class="btn btn-success" data-dismiss="modal">Czytaj więcej</button></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <a href="add_conference" class="btn btn-primary">Dodaj nową</a>
                </div>

            @else


            @endif


    </div>





