@include('flash')
@yield('flash')
@include('menu')


<script>
    $(document).ready(function() {

        $('.review_table').find('tr').click(function () {
            var idd = $(this).attr('id');
            var td = $('#roll-' + idd + ' td');
            //alert(cb.attr('checked'));


            if($(this).hasClass('expand')) {
                $(this).removeClass('expand');
                td.addClass('hiddenRow');
                $(this).find('span').addClass('glyphicon-chevron-down');
                $(this).find('span').removeClass('glyphicon-chevron-up');
            }else {
                $(this).addClass('expand');
                td.removeClass('hiddenRow');
                //alert(td.find('span').removeClass('glyphicon-chevron-down'));
                $(this).find('span').removeClass('glyphicon-chevron-down');
                $(this).find('span').addClass('glyphicon-chevron-up');
            }
        });

    });
</script>

<body>
    <div class="container">
         <h2>{!! $submission->title !!}</h2>
            <div class="article">
                <div class="article-info">
                    <h5 class="list-group-item-heading"><strong>Autorzy:</strong> {!! $submission->authors !!}</h5>
                    <h5 class="list-group-item-heading"><strong>Organizacja:</strong> {!! $submission->organization !!}</h5>
                    <p class="list-group-item-text">{!! $submission->describe_text !!}</p>
                </div>
                <div class="article-raport">
                    <a href="{!! URL::to($submission->file_path) !!}"><button class="btn btn-info">Raport pdf</button></a>
                    <a><button class="btn btn-warning" data-toggle="modal" data-target="#article-modal">Tajna wiadomość</button></a>
                </div>
            </div>



            <div class="article-comments list-group-item">
                <table class="table table-striped">
                    <thead class="review_table">
                        <tr>
                            <th>ID recenzji</th>
                            <th>Treść recenzji</th>
                            <th>Dodana przez</th>
                            <th>Data</th>
                            <th>Ocena recenzji</th>
                            <th>ocena/doświadczenia</th>
                            <!--<th>Decyzja T/N</th>-->
                        </tr>
                    </thead>
                    <tbody class="review_table">
                            @foreach($reviews as $review)
                                <tr id="{!! $review->id_review !!}" style="cursor: pointer">
                                    <td id="reviewID"><span class="glyphicon glyphicon-chevron-down"></span> {!! $review->id_review !!}</td>
                                    <td id="reviewOpinion">{!! $review->opinion !!}</td>
                                    <td id="reviewUser">
                                        {!! \App\Person::where('id_user', $review->id_uzytkownika)->first()->name !!}
                                        {!! \App\Person::where('id_user', $review->id_uzytkownika)->first()->lastname !!}
                                    </td>
                                    <td id="reviewDate">{!! $review->date !!}</td>
                                    <td id="reviewRate">{!! $review->rating !!}</td>
                                    <td id="rate-experience">{!! ($review->rating * $review->experience_reviewer) / $review->experience_reviewer !!}</td>
                                    <!--<td id="reviewDecision">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('agree', 'yes', null, array('id' => 'agree')) !!}
                                            </label>
                                        </div>
                                    </td>-->
                                </tr>
                                <tr id="roll-{!! $review->id_review !!}" class="roll">
                                    <td colspan="7" class="hiddenRow">{!! $review->opinion !!}</td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>

        @if(count($committee_member) != 0 and count($userReview) == 0 and \Carbon\Carbon::now() < $submission->deadline)
            <div class="panel">
            <div class="article-addComments">
                {!! Form::open(array('url' => array('review', $submission->id), 'class' => 'form-horizontal form-review')) !!}
                <fieldset>
                    <div class="form-group">
                        <label for="opinion" class="col-lg-2 control-label">Treść recenzji</label>
                        <div class="input-form input-author col-lg-8">{!! Form::textarea('opinion', null, array('class' => 'form-control', 'id' => 'author')) !!}</div>
                    </div>
                    <div class="form-group">
                        <label for="attention" class="col-lg-2 control-label">Poufne uwagi dla PC</label>
                        <div class="input-form input-author col-lg-8">{!! Form::textarea('attention', null, array('class' => 'form-control', 'id' => 'author', 'rows' => 3)) !!}</div>
                    </div>
                    <div class="form-group">
                        <label for="rate" class="col-lg-2 control-label">Ocena końcowa</label>
                        <div class="form-group">
                            <div class="col-lg-3">
                                {!! Form::select('rate', [3 => 'Mocna akceptacja', 2 => 'Akceptacja', 1 => 'Słaba akceptacja', 0 => 'Neutralny', -1 => 'Słabe odrzucenie', -2 => 'Odrzucenie', -3 => 'Mocne odrzucenie'], null, ['class' => 'form-control'] ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="experience" class="col-lg-2 control-label">Doświadczenie recenzenta</label>
                        <div class="form-group">
                            <div class="col-lg-3">
                                {!! Form::select('experience', [5 => 'Ekspert', 4 => 'Doświadczony', 3 => 'Średnie', 2 => 'Niskie', 1 => 'Brak'], null, ['class' => 'form-control'] ) !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::submit('Dodaj recenzję', array('class' => 'btn btn-info')) !!}
                </fieldset>
                {!! Form::close() !!}
            </div>
                </div>
        @else

        @endif

        <div class="extendReview well" style="display: none"></div>
    </div>


    <div class="modal" id="article-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Wiadomości od użytkowników !</h4>
                </div>
                <div class="modal-body">
                    @if(isset($message))
                    @foreach($message as $msg)
                        <blockquote>
                            <p>{!! $msg->secret_attention !!}</p>
                            <small>Wysłane przez <cite title="Source Title">{!! $msg->login !!}</cite></small>
                        </blockquote>
                    @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Porzuć</button>
                </div>
            </div>
        </div>
    </div>
</body>