@include('flash')
@include('main')
@include('menu')


<link href="assets/css/select2.css" rel="stylesheet" />
<script src="assets/js/select2.min.js"></script>


@if(Auth::check())
    <div class="container">
        <div class="jumbotron">
            <h1>Konferencje</h1>
            <p>Za pomocą tej podstrony możesz utworzyć nowe wydarzenie, takie jak konferencję, wypełnij wszystkie wymagane pola z formularza i kliknij przycisk "żądanie". Jeśli wypełnisz wszystkie pola formularza poprawnie zostaniesz poinformowany mailowo o aktywacji wydarzenia w przeciwnym wypadku zostaniesz poinformowany mailowo. </p>
            @if(Auth::check()) @else <p><a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#loginModal">Zaloguj się !</a></p> @endif
        </div>
        <div class="well bs-component">

            {!! Form::open(array('url' => 'conference', 'class' => 'form-horizontal')) !!}
            <div class="form-group conference-row">
                <h2 class="form-header">Nazwa wydarzenia</h2>
                <div class="conference-name conference-col form-group">
                    {!! Form::label('title', 'Pełna nazwa konferencji.', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-4">{!! Form::text('title', null, array('class' => 'form-control', 'id' => 'name')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('description', 'Opis konferencji', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-8">{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 5, 'id' => "textAre"]) !!}</div>
                </div>
            </div>

            <div class="form-group conference-row">
                <h2 class="form-header">Infromacje o wydarzeniu</h2>
                <div class="conference-col form-group">
                    {!! Form::label('website', 'Strona internetowa', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('website', null, array('class' => 'form-control', 'id' => 'website')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('country', 'Kraj', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('country', null, array('class' => 'form-control', 'id' => 'country')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('town', 'Miasto', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('town', null, array('class' => 'form-control', 'id' => 'town')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('postcode', 'Kod pocztowy', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('postcode', null, array('class' => 'form-control', 'id' => 'postcode')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('street', 'Adres', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('street', null, array('class' => 'form-control', 'id' => 'name')) !!}</div>
                </div>

                <div class="conference-col date form-group" id="datePickerS">
                    {!! Form::label('begin', 'Rozpoczęcie wydarzenia', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3" style="display: flex">
                        <span class="input-group-addon add-on glyphicon-conference"><span class="glyphicon glyphicon-calendar"></span></span>
                        {!! Form::text('begin', null, array('class' => 'form-control', 'id' => 'begin')) !!}
                    </div>
                </div>
                <div class="conference-col date form-group" id="datePickerE">
                    {!! Form::label('end', 'Zakończenie wydarzenia', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3" style="display: flex">
                        <span class="input-group-addon add-on glyphicon-conference"><span class="glyphicon glyphicon-calendar glyphicon-conference"></span></span>
                        {!! Form::text('end', null, array('class' => 'form-control', 'id' => 'end')) !!}
                    </div>
                </div>
            </div>

            <div class="form-group conference-row">
                <h2 class="form-header">Dane organizatora</h2>
                <div class="conference-col form-group">
                    {!! Form::label('phone', 'Telefon kontaktowy do organizatora', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('phone', null, array('class' => 'form-control', 'id' => 'name')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('mail', 'Mail organizatora', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form input-name col-lg-3">{!! Form::text('mail', null, array('class' => 'form-control', 'id' => 'mail')) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('type', 'Pełnione role w wydarzeniu', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3">{!! Form::select('type[]', ['Organizator' => 'Organizator', 'Recenzent' => 'Recenzent', 'Członek' => 'Członek'], null, ['class' => 'js-example-basic-multiple', 'multiple' => 'multiple', 'id' => 'tag_list']) !!}</div>
                </div>
            </div>

            <div class="form-group conference-row">
                <h2 class="form-header">Pozostałe</h2>
                <div class="conference-col form-group">
                    {!! Form::label('category', 'Dyscypliny naukowe', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3">{!! Form::select('category[]', array('Informatyka', 'Medycyna', 'Fizyka', 'Matematyka', 'Logistyka'), null, ['class' => 'js-example-basic-multiple', 'multiple' => 'multiple', 'id' => 'tag_list']) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('note', 'Dodatkowe informacje o wydarzeniu', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3">{!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3, 'id' => "textAre"]) !!}</div>
                </div>
                <div class="conference-col conference_pozostale form-group">
                    {!! Form::label('committee', 'Członkowie komitetu', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3">{!! Form::select('committee[]', $users, null, ['class' => 'js-example-basic-multiple', 'multiple' => 'multiple', 'id' => 'tag_list']) !!}</div>
                </div>
                <div class="conference-col form-group">
                    {!! Form::label('invite', 'Zaproś innych', array('class' => 'control-label col-lg-3')) !!}
                    <div class="input-form col-lg-3">{!! Form::textarea('invite', null, ['class' => 'form-control', 'rows' => 4, 'id' => "area_mail"]) !!}</div>
                </div>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <div class="col-lg-2">
                    {!! Form::submit('ZAPISZ', array('class' => 'btn btn-primary')) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@else

@endif

<script type="text/javascript">
    $(".js-example-basic-multiple").select2();


    $('#datePickerS, #datePickerE')
            .datepicker({
                format: 'yyyy/mm/dd'
            }).on('changeDate', function(ev){
                $('#datePickerS, #datePickerE').datepicker('hide');
            });

    $('.conference-col input[type="checkbox"]').click(function () {

        var checked = $(this).is(':checked');
        if(checked) {
            $('.conference_pozostale textarea').removeClass('hidden');
        }
        else {
            $('.conference_pozostale textarea').addClass('hidden');
        }
    });

    $('#area_mail').tooltip({
        title: 'Dodaj adresy e-mail osób, które chcesz zaprosić do komitetu naciskając klawisz Enter aby dodać nową!',
        placement: 'bottom'
    })

</script>
