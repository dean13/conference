@include('flash')
@include('main')
@include('menu')

<link href="http://fonts.googleapis.com/css?family=Roboto:300&amp;subset=latin-ext" rel="stylesheet">
<script src="//maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>
<script src="{!! URL::asset('assets/js/gmap3.min.js') !!}"></script>

<script>
    function joinToConference() {
        $.ajax({
            method: 'get',
            url: '{!! URL::to('join') !!}',
            data: {conference: '{!! $conf_info->id !!}'},
            success: (function ($data) {
                //$('.join-list').empty().fadeOut(500);
                $('.join-list').fadeOut(300, function() {
                    $(this).html('<span class="label label-success">Uczestniczysz już w tej konferencji</span>');
                    $(this).fadeIn(300);
                });
            }),
        })
    }

    function initializeReport() {
        $.ajax({
            method: 'get',
            url: '{!! URL::to('check_decision') !!}',
            data: {conference: '{!! $conf_info->id !!}'},
            success: (function ($data) {
                console.log($data);
                if($data=='1') {
                    $('.submission-message').html('<button type="button" class="btn btn-info" data-dismiss="modal" onclick="sendMessage();">Wyślij raport</button>');
                }
            }),
        })
    }

    function sendMessage() {
        $.ajax({
            method: 'get',
            url: '{!! URL::to('send_report') !!}',
            data: {conference: '{!! $conf_info->id !!}'},
            success: (function ($data) {
                $('.row-message').html('<div class="alert alert-flash alert-dismissible alert-success">' +
                        '<button type="button" class="close" data-dismiss="alert">x</button><p>Wiadomość wysłana !</p></div>').delay(3000).slideUp(300);
            })
        })
    }

    function applyDecision(decision, indexArticle) {
        $.ajax({
            method: 'get',
            url: '{!! URL::to('decision') !!}',
            data: {decision: decision, index: indexArticle},
            success: (function () {
            })
        })
    }

    $(document).ready(function () {
        var decision;
        var indexArticle;
        var index;
        var submissionForm = $("#saveSubmission");

        submissionForm.submit(function(e){
            e.preventDefault();
            var formData = submissionForm.serialize();
            $('#article-modal .warning li').remove();
            alert(formData.prop("file"));

            $.ajax({
                url:'{!! URL::to('submission',$id_conference) !!}',
                method:'post',
                data:{data: formData, file: $('#file').files},
                success:function(data){
                    $.each( data, function( key, value ) {
                        //$('#organization option[value="'+value+'"]').attr('selected','selected');
                        //console.log(key+" "+value);
                        $('#article-modal .warning ul').append('<li>'+value+'</li>')

                    });

                    //$('#article-modal .warning ul').append('<li>'+data+'</li>')
                },
                error: function (data) {
                    //console.log(data);
                }
            });
        });

        $("table tbody tr").click(function(){
            indexArticle = ($(this).find('td').html());
            index = this.rowIndex;
        });
        $('.decision-select').on('change', function() {
            decision = this.value;

            if(decision == 1) {
                //$(this).remove().delay(3000);
                $(this).fadeOut(500, function(){

                    $('table tbody tr:nth-child(' + index + ') td:nth-child(5)').remove();
                    $('table tbody tr:nth-child(' + index + ') td:nth-child(5)').html('<span class="glyphicon glyphicon-ok"></span>');

                    initializeReport();
                    applyDecision(decision, indexArticle);

                });
            }
            else if(decision == 2) {
                $(this).fadeOut(500, function(){
                    $('table tbody tr:nth-child(' + index + ') td:nth-child(5)').remove();
                    $('table tbody tr:nth-child(' + index + ') td:nth-child(5)').html('<span class="glyphicon glyphicon-minus"></span>');

                    initializeReport();
                    applyDecision(decision, indexArticle);
                });
            }
        });


        $('#datePicker')
                .datepicker({
                    format: 'yyyy/mm/dd'
                }).on('changeDate', function(ev){
            $('#datePicker').datepicker('hide');
        });

    });
</script>

<body>
    <div class="container">
        <div class="page-header">
            <h1 id="navbar">Informacje o konferencji</h1>
            <div class="conference-category">
                @foreach(explode(',', $conf_info->category) as $category) <a href="{!! URL::to('start/kategoria', $category) !!}">{!! $category !!}</a>, @endforeach
            </div>
        </div>
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row conference_tile">
                <h3>{!! $conf_info->title !!}</h3>
            </div>

            <div class="row conference_description">
                {!! $conf_info->description !!}
            </div>

            <div class="row conference_podstawowe">
                <div class="content-podstawowe">
                    <div class="list-group">
                        <ul class="list-unstyled">
                            <li>
                                <h3>Podstawowe informacje</h3>
                                <h5><strong>Nazwa konferencji</strong> - {!! $conf_info->title !!}</h5>
                                <h5><strong>Nazwa konferencji</strong> - {!! $conf_info->website !!}</h5>
                                <h5><strong>Telefon do organizatora</strong> - {!! $conf_info->phone !!}</h5>
                                <h5><strong>Mail do organizatora</strong> - {!! $conf_info->mail !!}</h5>
                                <strong>Data:</strong> {!! $conf_info->begin !!} - {!! $conf_info->end !!}
                            </li>
                            <li class="join-list">
                                @if(\App\ConferenceMember::where('id_conference',$conf_info->id)->where('id_member',\Illuminate\Support\Facades\Auth::user()->id)->count() > 0 or \App\Http\Controllers\User\UserController::isOrganizator($conf_info->id) == true)
                                    <span class="label label-success">Uczestniczysz w tej konferencji</span>
                                @else
                                    <a href="#" class="btn btn-info btn-lg" style="margin: 2% 0" onclick="joinToConference()">Dołącz do konferencji <span class="glyphicon glyphicon-plus-sign" style="margin: 1%"></span></a>
                                @endif
                            </li>
                            <li>
                                <h3>Adres konferencji</h3>
                                <p>{!! $conf_info->street !!}, {!! $conf_info->town !!} {!! $conf_info->postcode !!}</p>
                                <div class="conference_map">
                                    <div id="map"></div>
                                    <script>
                                        $('#map').gmap3({
                                            marker:{
                                                address: "{!! $mapa !!}"
                                            },
                                            map:{
                                                options:{
                                                    zoom: 15
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </li>
                            @if($conf_info->note)
                                <li>
                                    <h3>Dodatkowe informacje</h3>
                                    {!! $conf_info->note !!}
                                </li>
                            @endif
                        </ul>

                    </div>
                </div>
            </div>

            <div class="section-submission">
                @if(\App\ConferenceMember::where('id_conference',$conf_info->id)->where('id_member',\Illuminate\Support\Facades\Auth::user()->id)->first() or \App\Http\Controllers\User\UserController::isOrganizator($conf_info->id) == true)
                    <div>
                        <h1 id="navbar">Artykuły</h1>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#article-modal">
                            Dodaj
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                    </div>
                    <div class="row">
                        <div class="page-header">
                            <h1 id="navbar"></h1>
                        </div>

                        <div class="row-message"></div>

                        <div class="list-group list-submission">
                            <table class="table table-hover">
                                <thead class="review_table">
                                <tr>
                                    <th>ID Artykułu</th>
                                    <th>Tytuł</th>
                                    <th>Deadline</th>
                                    <th>Autorzy</th>
                                    <th style="text-align: center">Decyzja</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="submission_table">
                                    @foreach($submissions as $submission)
                                        <tr style="cursor: pointer">
                                            <td>{!! $submission->id !!}</td>
                                            <td><a href="{!! URL::to('conference/submission', $submission->id) !!}" style="text-decoration: none"><strong>{!! $submission->title !!}</strong></a></td>
                                            <td class="col-lg-2">{!! $submission->deadline !!}</td>
                                            <td>{!! $submission->authors !!}</td>
                                            <td class="col-lg-2" style="text-align: center">
                                                @if($submission->deadline < \Carbon\Carbon::now() and $submission->decision == null)
                                                    <select class="form-control decision-select" id="select">
                                                        <option class="active">Nieustalona</option>
                                                        <option value="1">Zatwierdzony</option>
                                                        <option value="2">Odrzucony</option>
                                                    </select>
                                                @elseif($submission->decision != null)
                                                    @if($submission->decision == 1)
                                                        <span class="glyphicon glyphicon-ok"></span>
                                                    @else
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    @endif
                                                @else
                                                    Oczekiwanie na DEADLINE
                                                @endif
                                            </td>
                                            <td class="decision-send">

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="submission-message">
                                    @if(isset($submission->decision) != null and \App\Http\Controllers\User\UserController::isOrganizator($conf_info->id) == true)
                                        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="sendMessage();">Wyślij raport</button>
                                    @endif
                            </div>
                        </div>

                        <div class="page-header">
                            <h1 id="navbar"></h1>
                        </div>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="modal" id="article-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nowy artykuł</h4>
                            </div>
                            <div class="modal-body">

                                {!! Form::open(array('url' => array('submission', $id_conference), 'class' => 'form-horizontal', 'id' => 'saveSubmission', 'novalidate' => 'novalidate', 'files' => true)) !!}
                                <fieldset>
                                    <div class="form-group">
                                        {!! Form::label('title', 'Pełny tytuł', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-title col-lg-8">{!! Form::text('title', null, array('class' => 'form-control', 'id' => 'title')) !!}</div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('describe', 'Krótka notatka', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-title col-lg-8">{!! Form::textarea('describe', null, array('class' => 'form-control', 'id' => 'describe')) !!}</div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('author', 'Autor', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-author col-lg-8">{!! Form::text('author', null, array('class' => 'form-control', 'id' => 'author')) !!}</div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('organization', 'Organizacja', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-organization col-lg-8">{!! Form::text('organization', null, array('class' => 'form-control', 'id' => 'organization')) !!}</div>
                                    </div>
                                    <div class="form-group form-surname date" id="datePicker">
                                        {!! Form::label('deadline', 'Deadline', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-birthday col-lg-6">
                                            {!! Form::text('deadline', null, array('class' => 'form-control', 'id' => 'birth_day')) !!}
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('file', 'Załącz plik', array('class' => 'control-label col-lg-3')) !!}
                                        <div class="input-form input-file col-lg-8">{!! Form::file('file', null, array('class' => 'form-control', 'id' => 'file')) !!}</div>
                                    </div>

                                    <div class="warning">
                                        <ul class="list-unstyled">

                                        </ul>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Porzuć</button>
                                {!! Form::submit('DODAJ', array('class' => 'btn btn-primary')) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</body>