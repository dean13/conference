<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed' => 'B��dny login lub has�o.',
    'throttle' => 'Za du�o nieudanych pr�b logowania. Prosz� spr�bowa� za :seconds sekund.',
];